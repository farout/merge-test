package misc

import (
	"log"
)

func main() {} // main ()

// ErrorPanic checks for errors and panics if there is any error.
func ErrorPanic(e error) {
	if e != nil {
		log.Panic(e) // panic for any errors.
	} // if
} // ErrorPanic ()
