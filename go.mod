module gitlab.com/bluebottle/merge-test

go 1.14

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/go-git/go-git/v5 v5.1.0
	github.com/pelletier/go-toml v1.2.0
)
